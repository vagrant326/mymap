package com.example.vagrant.mymap.Track;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.vagrant.mymap.R;;

import java.util.List;

public class TrackAdapter extends ArrayAdapter<Track> {
    public TrackAdapter(Context context, List<Track> tracks) {
        super(context, 0, tracks);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Track track = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.track_list_item, parent, false);
        }
        // Lookup view for data population
        TextView nazwa = (TextView) convertView.findViewById(R.id.tvNazwa);
        // Populate the data into the template view using the data object
        nazwa.setText(String.valueOf(track.getName()));
        // Return the completed view to render on screen
        return convertView;
    }
}