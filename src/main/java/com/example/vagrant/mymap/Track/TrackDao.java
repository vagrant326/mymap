package com.example.vagrant.mymap.Track;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.arch.lifecycle.LiveData;

import java.util.List;

@Dao
public interface TrackDao {
    @Query("SELECT * FROM track WHERE deleted = 0")
    public List<Track> getAll();

    @Query("SELECT * FROM track WHERE deleted = 0")
    public LiveData<List<Track>> getAllLive();

    @Query("SELECT * FROM Track WHERE uid LIKE :uid LIMIT 1")
    public Track findByName(int uid);

    @Query("SELECT * FROM track WHERE deleted = 0 LIMIT 1")
    public Track getFirst();

    @Query("SELECT * FROM track WHERE sent = 0")
    public List<Track> getUnsentTracks();

    @Insert
    public Long insert(Track track);

    @Insert
    public Long[] insertAll(List<Track> tracks);

    @Update
    public void update(Track track);

    @Update
    public void updateMany(List<Track> tracks);

    @Delete
    public void delete(Track track);
}
