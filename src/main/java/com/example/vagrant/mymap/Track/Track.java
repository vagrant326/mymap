package com.example.vagrant.mymap.Track;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

@Entity
public class Track {
    @PrimaryKey(autoGenerate = true)
    private Long uid;
    private String name;
    private boolean sent;
    private boolean deleted;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public boolean getDeleted() { return deleted; }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Track(Date date)
    {
        this();
        name = date.toString();
    }

    public Track()
    {
        this.sent = false;
        this.deleted = false;
    }
}
