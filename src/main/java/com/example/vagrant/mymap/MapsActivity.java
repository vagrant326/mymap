package com.example.vagrant.mymap;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteConstraintException;
import android.location.Location;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.LogPrinter;
import android.util.Printer;
import android.view.View;
import android.widget.Button;

import com.example.vagrant.mymap.Track.Track;
import com.example.vagrant.mymap.Waypoint.Waypoint;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    // Controls
    private GoogleMap mMap;
    private Button lista;
    private Button newTrack;
    private Button stopTrack;
    private Button sync;
    // Database object
    private DB database;

    private Track activeTrack;

    // Location data
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;

    // Sync data
    // The authority for the sync adapter's content provider
    public static final String AUTHORITY = "com.example.vagrant.mymap.provider";
    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "example.com";
    // The account name
    public static final String ACCOUNT = "dummyaccount";
    // Instance fields
    Account mAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Create the dummy account for sync
        mAccount = CreateSyncAccount(this);

        // Check permission to use location
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
            checkPermission();
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Init DB
        database = DB.getDB(this);
        // Create client to access device location
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        final Context object = this;
        // Init button opening "List" activity
        lista = (Button) findViewById(R.id.openListButton);
        lista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent(object, TracksList.class);
                startActivityForResult(data, 0);
            }
        });

        // Init button starting a new track
        newTrack = (Button) findViewById(R.id.startTrack);
        newTrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Remove all existing markers
                mMap.clear();
                // Create new track with actual date as a name
                activeTrack = new Track(new Date());
                activeTrack.setUid(database.trackDao().insert(activeTrack));
                // Switch START/STOP buttons
                newTrack.setVisibility(View.INVISIBLE);
                stopTrack.setVisibility(View.VISIBLE);

                try {
                    mLocationCallback = InitLocationCallback();
                    // Start tracking location
                    mFusedLocationClient.requestLocationUpdates(InitLocationRequest(), mLocationCallback,null);
                }
                catch (SecurityException ex)
                {
                    return;
                }
            }
        });

        // Init button which stops tracking
        stopTrack = (Button) findViewById(R.id.stopTrack);
        stopTrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeTrack = null;
                // Switch START/STOP buttons
                newTrack.setVisibility(View.VISIBLE);
                stopTrack.setVisibility(View.INVISIBLE);
                // Stop tracking location
                mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            }
        });

        // Init button which syncs data
        sync = (Button) findViewById(R.id.syncButton);
        sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Pass the settings flags by inserting them in a bundle
                Bundle settingsBundle = new Bundle();
                settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, false);
                /*
                 * Request the sync for the default account, authority, and
                 * manual sync settings
                 */
                ContentResolver.requestSync(mAccount, AUTHORITY, settingsBundle);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK)
        {
            // Remove all existing markers
            mMap.clear();
            // Get id of track selected in "List" activity
            Long trackId = data.getLongExtra("id",-1);
            // Fetch all waypoints of this track
            List<Waypoint> waypoints = database.waypointDao().getAllOfTrack(trackId);
            // For every waypoint, create marker on map
            LatLng punkt = new LatLng(0,0);
            for (int i=0; i<waypoints.size(); i++)
            {
                punkt = new LatLng(waypoints.get(i).dlugosc, waypoints.get(i).szerokosc);
                mMap.addMarker(new MarkerOptions().position(punkt));
            }
            mMap.moveCamera(CameraUpdateFactory.newLatLng(punkt));
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    // Check permission to use device's location and ask for it if needed
    public void checkPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {//Can add more as per requirement
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},123);
        }
    }

    public void SaveAndShowLocation(Location location)
    {
        // Add waypoint to DB
        database.waypointDao().insert(new Waypoint(location.getLongitude(), location.getLatitude(), activeTrack.getUid()));
        // Create marker on map
        LatLng punkt = new LatLng(location.getLongitude(), location.getLatitude());
        mMap.addMarker(new MarkerOptions().position(punkt));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(punkt));
    }

    public LocationRequest InitLocationRequest()
    {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    public LocationCallback InitLocationCallback()
    {
        return new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                // For every location buffered
                for (Location location : locationResult.getLocations()) {
                    SaveAndShowLocation(location);
                }
            }
        };
    }

    /**
     * Create a new dummy account for the sync adapter
     *
     * @param context The application context
     */
    public static Account CreateSyncAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(
                ACCOUNT, ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (!accountManager.addAccountExplicitly(newAccount, null, null)) {
            Log.d("account creation", "addAccount Failed");
        }
        return newAccount;
    }
}
