package com.example.vagrant.mymap;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class NewWaypoint extends AppCompatActivity {

    private Button zapisz;
    private Button anuluj;
    private EditText dlugosc;
    private EditText szerokosc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_waypoint);
        dlugosc = (EditText) findViewById(R.id.dlugosc);
        szerokosc = (EditText) findViewById(R.id.szerokosc);
        zapisz = (Button) findViewById(R.id.zapisz);
        zapisz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = getIntent();
                //---set the data to pass back---
                data.putExtra("dlugosc", dlugosc.getText().toString());
                data.putExtra("szerokosc", szerokosc.getText().toString());
                setResult(RESULT_OK, data);
                //---close the activity---
                finish();
            }
        });
        anuluj = (Button) findViewById(R.id.anuluj);
        anuluj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

}
