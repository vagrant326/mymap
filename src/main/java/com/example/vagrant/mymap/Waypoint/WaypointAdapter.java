package com.example.vagrant.mymap.Waypoint;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.vagrant.mymap.R;

import java.util.List;

public class WaypointAdapter extends ArrayAdapter<Waypoint> {
    public WaypointAdapter(Context context, List<Waypoint> waypoints) {
        super(context, 0, waypoints);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Waypoint waypoint = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.waypoint_list_item, parent, false);
        }
        // Lookup view for data population
        TextView tvSzerokosc = (TextView) convertView.findViewById(R.id.tvSzerokosc);
        TextView tvDlugosc = (TextView) convertView.findViewById(R.id.tvDlugosc);
        // Populate the data into the template view using the data object
        tvSzerokosc.setText(String.valueOf(waypoint.szerokosc));
        tvDlugosc.setText(String.valueOf(waypoint.dlugosc));
        // Return the completed view to render on screen
        return convertView;
    }
}