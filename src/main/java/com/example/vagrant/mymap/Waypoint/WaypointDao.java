package com.example.vagrant.mymap.Waypoint;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.arch.lifecycle.LiveData;

import java.util.List;

@Dao
public interface WaypointDao {

    @Query("SELECT * FROM waypoint")
    public List<Waypoint> getAll();

    @Query("SELECT * FROM waypoint")
    public LiveData<List<Waypoint>> getAllLive();

    @Query("SELECT * FROM waypoint WHERE uid LIKE :uid LIMIT 1")
    public Waypoint findByName(int uid);

    @Query("SELECT * FROM waypoint LIMIT 1")
    public Waypoint getFirst();

    @Query("SELECT * FROM waypoint WHERE trackId = :trackId")
    public List<Waypoint> getAllOfTrack(Long trackId);

    @Insert
    public Long insert(Waypoint waypoint);

    @Insert
    public Long[] insertAll(List<Waypoint> waypoints);

    @Update
    public void update(Waypoint waypoint);

    @Delete
    public void delete(Waypoint waypoint);
}
