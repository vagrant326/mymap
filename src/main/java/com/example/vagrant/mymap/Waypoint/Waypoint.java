package com.example.vagrant.mymap.Waypoint;

import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Entity;

import com.example.vagrant.mymap.Track.Track;

import java.util.Date;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(
    foreignKeys =
    @ForeignKey(
            entity = Track.class,
            parentColumns = "uid",
            childColumns = "trackId",
            onDelete = CASCADE
    ),
        indices = {@Index(value = {"trackId"})}
)
public class Waypoint {
    @PrimaryKey(autoGenerate = true)
    public Long uid;
    public float szerokosc;
    public float dlugosc;
    public Long trackId;
    public String timestamp;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public float getSzerokosc()
    {
        return szerokosc;
    }

    public void setSzerokosc(float szerokosc)
    {
        this.szerokosc = szerokosc;
    }

    public float getDlugosc() {
        return dlugosc;
    }

    public void setDlugosc(float dlugosc) {
        this.dlugosc = dlugosc;
    }

    public Long getTrackId() {
        return trackId;
    }

    public void setTrackId(Long trackId) {
        this.trackId = trackId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Waypoint(String dlugosc, String szerokosc)
    {
        this();
        this.dlugosc = Float.parseFloat(dlugosc);
        this.szerokosc = Float.parseFloat(szerokosc);
    }

    public Waypoint(Long uid, float dlugosc, float szerokosc)
    {
        this();
        this.szerokosc = szerokosc;
        this.dlugosc = dlugosc;
        this.uid = uid;
    }

    public Waypoint(double dlugosc, double szerokosc, Long trackId)
    {
        this();
        this.szerokosc =  (float)szerokosc;
        this.dlugosc = (float)dlugosc;
        this.uid = uid;
        this.trackId = trackId;
    }

    private Waypoint()
    {
        Date date = new Date();
        this.timestamp = date.toString();
    }
}
