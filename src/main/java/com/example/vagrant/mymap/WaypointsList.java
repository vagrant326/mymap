package com.example.vagrant.mymap;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.vagrant.mymap.Waypoint.Waypoint;
import com.example.vagrant.mymap.Waypoint.WaypointAdapter;

import java.util.List;

public class WaypointsList extends AppCompatActivity {
    private List<Waypoint> waypoints;
    WaypointAdapter adapter;
    DB database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waypoints_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        database = DB.getDB(this);
        waypoints = database.waypointDao().getAll();
        ListView listview = (ListView)findViewById(R.id.waypointsList);
        adapter=new WaypointAdapter(this, waypoints);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> listview, View view, int position, long id)
            {
                Intent intent = getIntent();
                intent.putExtra("dlugosc", waypoints.get(position).dlugosc);
                intent.putExtra("szerokosc", waypoints.get(position).dlugosc);
                intent.putExtra("nazwa", position);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        final Context object = this;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(object, NewWaypoint.class);
                startActivityForResult(intent, 0);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK)
        {
            Waypoint newWaypoint = new Waypoint(data.getStringExtra("dlugosc"), data.getStringExtra("szerokosc"));
            adapter.add(newWaypoint);
            database.waypointDao().insert(newWaypoint);
            waypoints = database.waypointDao().getAll();
        }
    }
}
