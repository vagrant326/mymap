package com.example.vagrant.mymap;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.vagrant.mymap.Track.Track;
import com.example.vagrant.mymap.Track.TrackDao;
import com.example.vagrant.mymap.Waypoint.Waypoint;
import com.example.vagrant.mymap.Waypoint.WaypointDao;

@Database(entities = {Waypoint.class, Track.class}, version = 1)
public abstract class DB extends RoomDatabase {
    private static DB INSTANCE;
    public abstract WaypointDao waypointDao();
    public abstract TrackDao trackDao();

    public static DB getDB(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), DB.class, "WaypointDB")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }
    public static void destroyInstance() {
        INSTANCE = null;
    }
}
