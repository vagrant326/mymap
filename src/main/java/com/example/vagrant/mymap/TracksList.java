package com.example.vagrant.mymap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.vagrant.mymap.Track.Track;
import com.example.vagrant.mymap.Track.TrackAdapter;

import java.util.List;

public class TracksList extends AppCompatActivity {
    private List<Track> tracks;
    TrackAdapter adapter;
    DB database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracks_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        database = DB.getDB(this);
        tracks = database.trackDao().getAll();
        ListView listview = (ListView)findViewById(R.id.tracksList);
        adapter=new TrackAdapter(this, tracks);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> listview, View view, int position, long id)
            {
                Intent intent = getIntent();
                intent.putExtra("id", tracks.get(position).getUid());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        registerForContextMenu(listview);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId()==R.id.tracksList) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.list_ctx_menu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if (item.getItemId() == R.id.delete)
        {
            Track trackToDelete = tracks.get(info.position);
            trackToDelete.setDeleted(true);
            trackToDelete.setSent(false);
            database.trackDao().update(trackToDelete);
            adapter.remove(trackToDelete);
            return true;
        }
        else
        {
            return super.onContextItemSelected(item);
        }
    }
}
