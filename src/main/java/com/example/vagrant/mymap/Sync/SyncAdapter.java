package com.example.vagrant.mymap.Sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.vagrant.mymap.DB;
import com.example.vagrant.mymap.Track.Track;
import com.example.vagrant.mymap.Waypoint.Waypoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Handle the transfer of data between a server and an
 * app, using the Android sync adapter framework.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {
    // Global variables
    // Define a variable to contain a content resolver instance
    ContentResolver mContentResolver;

    // Database object
    private DB database;

    private Context ctx;

    /**
     * Set up the sync adapter
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        ctx = context;
        mContentResolver = context.getContentResolver();
    }

    /**
     * Set up the sync adapter. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public SyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        ctx = context;
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
    }

    /*
    * Specify the code you want to run in the sync adapter. The entire
    * sync adapter runs in a background thread, so you don't have to set
    * up your own background processing.
    */
    @Override
    public void onPerformSync(
            Account account,
            Bundle extras,
            String authority,
            ContentProviderClient provider,
            SyncResult syncResult) {

        // Init DB
        database = DB.getDB(ctx);
        JSONObject object = new JSONObject();
        JSONArray tracksJSON = new JSONArray();
        JSONArray waypointsJSON = new JSONArray();
        try{
            object.put("tracks", tracksJSON);
            object.put("waypoints", waypointsJSON);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        //temp
        List<Track> temptracks = database.trackDao().getAll();
        for (Track track :temptracks)
        {
            track.setSent(false);
            database.trackDao().update(track);
        }
        //-temp
        final List<Track> tracks = database.trackDao().getUnsentTracks();
        for (Track track : tracks)
        {
            JSONObject trackJSON = new JSONObject();
            try
            {
                trackJSON.put("uid", track.getUid());
                trackJSON.put("name", track.getName());
                trackJSON.put("deleted", track.getDeleted());
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            tracksJSON.put(trackJSON);

            List<Waypoint> waypoints = database.waypointDao().getAllOfTrack(track.getUid());
            for (Waypoint waypoint : waypoints)
            {
                JSONObject waypointJSON = new JSONObject();
                try
                {
                    waypointJSON.put("uid", waypoint.getUid());
                    waypointJSON.put("szerokosc", waypoint.getSzerokosc());
                    waypointJSON.put("dlugosc", waypoint.getDlugosc());
                    waypointJSON.put("trackId", waypoint.getTrackId());
                    waypointJSON.put("timestamp", waypoint.getTimestamp());
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                waypointsJSON.put(waypointJSON);
            }
        }
        RequestQueue queue = Volley.newRequestQueue(ctx);
        String url ="http://vagrant326.ct8.pl/MyMapApi/write.php";
        Log.d("JSON", object.toString());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, object,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    for (Track track : tracks) {
                        if (track.getDeleted()) {
                            database.trackDao().delete(track);
                        }
                        else {
                            track.setSent(true);
                            database.trackDao().update(track);
                        }
                    }
                    Log.d("Sync", "HTTP OK");
                    Log.d("Sync", response.toString());
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    for (Track track : tracks)
                    {
                        track.setSent(false);
                        database.trackDao().update(track);
                    }
                    Log.d("Sync", "HTTP Error");
                    Log.d("Sync", error.getMessage());
                    Log.d("Sync", error.toString());
                }
            });
        queue.add(jsonRequest);
    }
}